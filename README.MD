# IMPORTANT

- Tested work on: 
	- "react": "16.9.0"
    - "react-native": "0.61.5"

- Tested not work on (network error when using form data):
	- "react": "16.11.0"
	- "react-native": "0.62.2"

# TODO

- Check input

- Test again through apirequest

# HOW TO USE

- Install **axios**:

> npm -i axios

- Copy the file: **eyeq_<service_name>.js** to any location you want.

- Import the module:

  

**Note:** just import the module once.

  

```
import eyeq_<service_name> from './path/to/file'
var eyeqService = eyeq_<service_name>()
```

  

For example:

```
import eyeq_ocr from './path/to/module/file'
var eyeqService = eyeq_ocr()
```
  

- Set token:

```
eyeqService.setToken('PUT_YOUR_TOKEN_HERE')
```
  

**Note:** just set the token once.

  

- See the document below for all the methods.

  

# METHODS

|Service name|Method|Input|Note|
|--|--|--|--|
|ocr|ocr|{name, uri,type: 'image/jpg'}||
|ekyc|match|{name: 'id', uri,type: 'image/jpg'},{name:'face', uri,type: 'image/jpg'}||
|ekyc|head-pose|{name: 'id', uri,type: 'video/mp4'}|**not test**|
|ekyc|predict-license|{name: 'id', uri,type: 'image/jpg'}|**not test**|

  

All the methods above returns a promise, you can get the result or catch the error by:

```
eyeqService.<method_name>(input).then(response => {....}).catch(err => {...})
```
  

**Note**

Using form data to construct the input.

Example:

```
var formData = new FormData()
// formData.append(key, obj)
formData.append('front', {name: '....', uri: '...', type: '....'})
```
  

# Full Example

```
class App extends React.Component {
	constructor(props) {
		super(props)
		this.eyeqService = eyeq_ocr()
	}
	getOcrResult = (uri) => {
		var  formData = new  FormData()
		this.eyeqService.ocr(formData)
		.then(res  => {console.log(res)})
		.catch(err  => {console.log(err)})
	}
	componentDidMount() {
		this.eyeqService.setToken("TOKEN_PLEASE")
	}
	render() {
		return ()
	}
}
