/*
EYEQ SAMPLE REQUEST

*/
import axios from 'axios'
const BASE_URL = 'https://apirequest.eyeq.tech'
export default eyeq_ocr = () => {
    var API_TOKEN = undefined
    var axiosAPI = undefined
    return {
        setToken: (token) => {
            if (API_TOKEN !== undefined) {
                throw "Api token is already set up, can't modify !"
            } 
            API_TOKEN = token
            axiosAPI = axios.create({
                headers: {
                    Authorization: `Bearer ${API_TOKEN}`,
                    'Content-Type': 'multipart/form-data',
                    'Accept': '*/*'
                }
            })
        },
        ocr: (data) => axiosAPI.post(BASE_URL + '/rec/ocr', data),
        
    }
}

